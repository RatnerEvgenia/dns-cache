from Query import Query


class QueryResponse(Query):
    def __init__(self, id, qr, flags, qd, an, ns, ar, query_type, query):
        super(QueryResponse, self).__init__(id, qr, flags, qd, an, ns, ar, query_type, query)
        self.ANSWERS = []
        self.AUTORITY_ANSWERS = []
        self.ADDITIONAL_ANSWERS = []
