import shelve

import time


class Cache:
    def __init__(self):
        self.db = shelve.open('cache')

    def get(self, packet):
        self.db = shelve.open('cache')
        query_key = str(packet.QueryType) + packet.Query
        answer = self.db.get(query_key).get('packet')
        self.db.close()
        return answer

    def put(self, parsed_packet):
        query_key = str(parsed_packet.QueryType) + parsed_packet.Query
        current_time = time.localtime()
        current_time_in_seconds = current_time.tm_hour * 3600 + current_time.tm_min * 60 + current_time.tm_sec
        query_value = {'packet': parsed_packet, 'time': current_time_in_seconds}  # packet содержит в себе AN, NS, AR
        self.db = shelve.open('cache')
        self.db[query_key] = query_value
        self.db.close()

    def contains_and_TTL(self, question):
        question_key = str(question.QueryType) + question.Query
        self.db = shelve.open('cache')
        if question_key in self.db.keys():
            self.db.close()
            return True
        self.db.close()
        return False

    def TTL_check(self, question):
        current_time = time.localtime()
        current_time_in_seconds = current_time.tm_hour * 3600 + current_time.tm_min * 60 + current_time.tm_sec
        self.db = shelve.open('cache')
        packet = self.db[question].get('packet')

        self.remove(packet.ANSWERS, current_time_in_seconds, self.db[question].get('time'))
        self.remove(packet.AUTORITY_ANSWERS, current_time_in_seconds, self.db[question].get('time'))
        self.remove(packet.ADDITIONAL_ANSWERS, current_time_in_seconds, self.db[question].get('time'))
        self.db.close()

        if (len(packet.ANSWERS) == 0) or (len(packet.AUTORITY_ANSWERS) == 0) or (len(packet.ADDITIONAL_ANSWERS) == 0):
            return False
        return True

    def remove(self, array, current_time_in_seconds, old_time):
        to_remove = []
        for answer in array:
            if (current_time_in_seconds - old_time) > answer.TTL:
                to_remove.append(answer)

        for answer in to_remove:
            array.remove(answer)
