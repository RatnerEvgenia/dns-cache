import copy

from Answer import Answer
from Query import Query
from QueryResponse import QueryResponse


class QueryParse():
    def __init__(self, query):
        self.bytes = query

        self.ID = int.from_bytes((query[0:2]), byteorder='big')
        self.QR = int.from_bytes((query[2:4]), byteorder='big') >> 15  # первый флаг, 0 - запрос, 1 - ответ
        self.FLAGS = query[2:4]  # если потребуются, потом декодировать
        self.QD = int.from_bytes((query[4:6]), byteorder='big')  # кол-во вопросов
        self.AN = int.from_bytes((query[6:8]), byteorder='big')
        self.NS = int.from_bytes((query[8:10]), byteorder='big')
        self.AR = int.from_bytes((query[10:12]), byteorder='big')
        self.TYPE = None

        self.QUERY_A = ''
        self.DomainNameLen = 0

    def query_parse(self):

        if (self.QR == 0):
            # if (self.TYPE == 1):  # запись A
            self.QUERY_A = self.domain_name_parse(self.bytes[12: - 4])
            self.TYPE = int.from_bytes(self.bytes[self.DomainNameLen + 16: self.DomainNameLen + 18],
                                       byteorder='big')
            return Query(self.ID, self.QR, self.FLAGS, self.QD, self.AN, self.NS, self.AR, self.TYPE,
                         self.QUERY_A)
        else:
            self.QUERY_A = self.domain_name_parse(self.bytes[12:len(self.bytes)])
            self.TYPE = int.from_bytes(self.bytes[self.DomainNameLen + 13: self.DomainNameLen + 15], byteorder='big')

            return self.answer_parse(self.bytes[self.DomainNameLen + 17: len(self.bytes)])

    def domain_name_parse(self, bytes):
        ip = 0
        AA = ''
        while (bytes[ip] != 0 and bytes[ip+1] != 0):
            length = bytes[ip]
            AA += bytes[ip + 1:ip + length + 1].decode("utf-8")
            AA += '.'
            ip += length + 1

        self.DomainNameLen = ip
        return AA

    def answer_parse(self, answer):

        response = QueryResponse(self.ID, self.QR, self.FLAGS, self.QD, self.AN, self.NS, self.AR, self.TYPE,
                                 self.QUERY_A)
        self.ip = 0
        self.an_count = copy.deepcopy(self.AN)
        self.ns_count = copy.deepcopy(self.NS)
        self.ar_count = copy.deepcopy(self.AR)

        while self.an_count > 0:
            response.ANSWERS.append(self.parse(answer[self.ip:len(answer)], 'AN'))
        while self.ns_count > 0:
            response.AUTORITY_ANSWERS.append(self.parse(answer[self.ip:len(answer)], 'NS'))
        while self.ar_count > 0:
            response.ADDITIONAL_ANSWERS.append(self.parse(answer[self.ip:len(answer)], 'AR'))

        return response

    def parse(self, answer, type):

        QUERY = b''
        self.NAME = answer[0: 2]
        self.ANS_TYPE = int.from_bytes((answer[2: 4]), byteorder='big')
        self.TTL = int.from_bytes((answer[6:  10]), byteorder='big')
        self.DATA_LENGTH = int.from_bytes((answer[10: 12]), byteorder='big')
        self.DATA = answer[12 :  (12 + self.DATA_LENGTH)]
        # QUERY = copy.deepcopy(self.DATA)

        # if (self.ANS_TYPE == 1 or self.ANS_TYPE == 28):
        #     # QUERY = self.ip_parse(self.DATA)
        #     # QUERY = copy.deepcopy(self.DATA)
        #     # self.ip += 4 + 12
        # if (self.ANS_TYPE == 5 or self.ANS_TYPE == 2):
        #     # QUERY = self.domain_name_parse(self.DATA)
        #     # self.ip += self.D + 12

        self.ip += 12 + self.DATA_LENGTH

        if (type == 'AN'):
            self.an_count -= 1
        if (type == 'NS'):
            self.ns_count -= 1
        if (type == 'AR'):
            self.ar_count -= 1

        ans = Answer()
        ans.NAME = self.NAME
        ans.ANS_TYPE = self.ANS_TYPE
        ans.CLASS = 1
        ans.TTL = self.TTL
        ans.DATA_LENGTH = self.DATA_LENGTH
        # ans.DATA = QUERY
        ans.DATA = self.DATA

        return ans

# qp = QueryParse(b'r\xe7\x81\x80\x00\x01\x00\x04\x00\x00\x00\x00\x08clients4\x06google\x03com\x00\x00\x01\x00\x01\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04W\xfa\xfaw\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04\xd5\xb4\xc1w\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04W\xfa\xfbw\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04]\x9e\x86w')
# # print(qp.domain_name_parse('\x02mc\x06yandex\x02ru\x00\x00\x01\x00\x01\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04W\xfa\xfaw\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04\xd5\xb4\xc1w\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04W\xfa\xfbw\xc0\x0c\x00\x01\x00\x01\x00\x00\x00\x9f\x00\x04]\x9e\x86w'))
# print(qp.query_parse().Query)