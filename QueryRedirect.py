import socket

from QueryParse import QueryParse


class QueryRedirect:
    def __init__(self, ip, query):
        self.Server = ip
        self.Query = query
        self.Answer = None
        self.a = None
        self.ans = None

    def send_query(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.sendto(self.Query, (self.Server, 53))
            try:
                self.ans = sock.recv(512)
            finally:
                if len(self.ans) == 0:
                    print('Вышестоящий сервер недоступен, запрос перенаправлен на другой сервер')
                    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock_2:
                        sock_2.sendto(self.Query, ('8.8.8.8', 53))
                        self.ans = sock_2.recv(512)
            self.Answer = bytearray(self.ans)

        qp = QueryParse(self.Answer)
        parsed_answer = qp.query_parse()

        return {'parsed_response': parsed_answer, 'response': self.ans}
