import re
import struct


class PacketCreater:
    def __init__(self, parsed_packet):
        self.Parsed_packet = parsed_packet
        self.Result = b''

    def create_packet(self):

        self.Result += self.remove_excess_zeros(struct.pack('>i', self.Parsed_packet.ID))
        self.Result += self.Parsed_packet.FLAGS
        self.Result += self.remove_excess_zeros(struct.pack('>I', self.Parsed_packet.QD))
        self.Result += self.remove_excess_zeros(struct.pack('>I', self.Parsed_packet.AN))
        self.Result += self.remove_excess_zeros(struct.pack('>I', self.Parsed_packet.NS))
        self.Result += self.remove_excess_zeros(struct.pack('>I', self.Parsed_packet.AR))
        self.Result += self.domain_name_encode(self.Parsed_packet.Query)
        self.Result += self.remove_excess_zeros(struct.pack('>I', self.Parsed_packet.QueryType))
        self.Result += self.remove_excess_zeros(struct.pack('>I', self.Parsed_packet.Class))

        if len(self.Parsed_packet.ANSWERS) != 0:
            self.encode_answers(self.Parsed_packet.ANSWERS)
        if len(self.Parsed_packet.AUTORITY_ANSWERS) != 0:
            self.encode_answers(self.Parsed_packet.AUTORITY_ANSWERS)
        if len(self.Parsed_packet.ADDITIONAL_ANSWERS) != 0:
            self.encode_answers(self.Parsed_packet.ADDITIONAL_ANSWERS)

        return self.Result

    def encode_answers(self, answers):
        for answer in self.Parsed_packet.ANSWERS:
            self.Result += answer.NAME
            self.Result += self.remove_excess_zeros(struct.pack('>I', answer.ANS_TYPE))
            self.Result += self.remove_excess_zeros(struct.pack('>I', answer.CLASS))
            self.Result += struct.pack('>I', answer.TTL)
            self.Result += self.remove_excess_zeros(struct.pack('>I', answer.DATA_LENGTH))
            self.Result += answer.DATA

    def remove_excess_zeros(self, bytes):
        if bytes[0] == 0 and bytes[1] == 0:
            return bytes[2:]

    def domain_name_encode(self, name):
        result = b''
        finder = re.findall('[^\\.]+', name)
        for level in finder:
            result += self.remove_excess_zeros(struct.pack('>I', len(level))[1:])  # хз почему так
            result += level.encode('utf-8')
        result += self.remove_excess_zeros(struct.pack('>I', 0)[1:])

        return result
