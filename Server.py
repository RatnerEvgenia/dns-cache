import argparse
import socket

from Cache import Cache
from PacketCreater import PacketCreater
from QueryParse import QueryParse
from QueryRedirect import QueryRedirect

SERVER = ''
PORT = ''


def add_arguments():
    parser = argparse.ArgumentParser(description='Кэширующий DNS-сервер, '
                                                 'принимающий стандартные (не dig) query-пакеты с запросом типа A'
                                                 ' и формирующий ответные. '
                                                 'Запрос к вышестоящему серверу отправляется в том случае, '
                                                 'если все ответы хотя бы из одной секции устарели.'
                                                 ' При отказе вышестоящего сервера запрос перенаправляется на сервер'
                                                 ' 8.8.8.8. \n'
                                                 'Запускать на системе Linux(Ubuntu). '
                                                 'В файле etc/resolv.conf указать 127.0.0.1.')

    parser.add_argument("--port", "-p", type=int, default=53, action="store",
                        help="Задать слушаемый порт UDP (не менять)")

    parser.add_argument("-f", "--forwarder", type=str, default="77.88.8.8", action="store",
                        help="Задать адрес вышестоящего сервера")

    parser.add_argument("-r", "--run", default=False, action="store_true",
                        help="Запуск сервера")

    options = parser.parse_args()

    if options.forwarder == '127.0.0.1' or options.forwarder == 'localhost':
        print('Указан неверный адрес вышестоящего сервера')
    else:
        global SERVER
        SERVER = options.forwarder
    global PORT
    PORT = options.port

    if options.run:
        print('server is working')
        serv = Server()
        serv.cycle()


class Server:
    def __init__(self):
        self.Cache = Cache()

    def cycle(self):
        while True:
            self.listen_to()

    def listen_to(self):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.bind(('127.0.0.1', PORT))
            data, addr = sock.recvfrom(512)
            print(data)

            query = bytearray(data)
            qp = QueryParse(query)
            parsed_query = qp.query_parse()
            if not self.Cache.contains_and_TTL(parsed_query):
                print(addr[0], ', A', ', ', parsed_query.Query, ', forwarder')
                qr = QueryRedirect(SERVER, data)
                answer = qr.send_query()
                sock.sendto(answer.get('response'), addr)
                self.Cache.put(answer.get('parsed_response'))
            else:
                print(addr[0], ', A', ', ', parsed_query.Query, ', cache')
                pc = PacketCreater(self.Cache.get(parsed_query))
                sock.sendto(pc.create_packet(), addr)


add_arguments()
