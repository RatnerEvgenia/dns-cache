

class Query():
    def __init__(self, id, qr, flags, qd, an, ns, ar, query_type, query):
        self.ID = id
        self.QR = qr
        self.FLAGS = flags
        self.QD = qd
        self.AN = an
        self.NS = ns
        self.AR = ar

        self.QueryType = query_type
        self.Class = 1 #INternet
        self.Query = query

